FROM maven:3.8.4-openjdk-17

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn clean install package

ENV PORT 8000
EXPOSE $PORT

CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} -Dspring.security.oauth2.client.registration.google.client-id=${GOOGLE_CLIENT_ID} -Dspring.security.oauth2.client.registration.google.client-secret=${GOOGLE_CLIENT_SECRET}  spring-boot:run" ]
