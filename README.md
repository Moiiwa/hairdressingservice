#How to build and run

##Build

###Vanilla build with maven
In case you want to run project from the jar, the first thing you have to do, is to change application.properties.

Since you are not going to use docker, you should make sure, that you have PostgreSQL installed on your machine, and database,
specified in the application.properties exists.
Then you have to change host, specified in the database path to either ```localhost```, or ```0.0.0.0``` or ```127.0.0.1```.
Then you go with 
```
mvn clean install package
```
###Build with docker

run
```
docker-compose up
```
##Run
###Vanilla run with maven
After building project with maven, you can go to the ```target``` directory and run
```
java -jar *jar package name*
```
Or you can do the same steps with your favorite IDE, with adding new configuration.