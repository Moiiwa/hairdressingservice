package hairdressing_svc.service;

import hairdressing_svc.entity.BarberEntity;
import hairdressing_svc.entity.BarbershopEntity;
import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.repository.BarberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BarbersService {

    private final BarberRepository repository;

    public void registerBarber(UserEntity user, BarbershopEntity barbershop){
        var barber = new BarberEntity();
        barber.setName(user.getName());
        barber.setUser(user);
        barber.setBarbershop(barbershop);
        repository.save(barber);
    }

    public BarberEntity getBarberById(Integer id){
        return repository.getBarberEntityByBarberId(id);
    }
}
