package hairdressing_svc.service;

import hairdressing_svc.dto.UserRegisterDto;
import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.exception.UsernameTakenException;
import hairdressing_svc.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserEntity createUser(UserRegisterDto userRegister) {
        if (userRepository.existsByUsername(userRegister.getUsername()))
            throw new UsernameTakenException("User with such username already exist");
        UserEntity user = UserEntity.builder()
                .username(userRegister.getUsername())
                .password(passwordEncoder.encode(userRegister.getPassword()))
                .name(userRegister.getName())
                .surname(userRegister.getSurname())
                .phoneNumber(userRegister.getPhoneNumber())
                .role(UserEntity.Role.ROLE_USER)
                .provider(UserEntity.Provider.LOCAL)
                .build();
        return userRepository.save(user);
    }

    public UserEntity generateAnonUser() {
        UserEntity user = UserEntity.builder()
                .username(UUID.randomUUID().toString())
                .password(passwordEncoder.encode(UUID.randomUUID().toString()))
                .provider(UserEntity.Provider.LOCAL)
                .role(UserEntity.Role.ROLE_ANON_USER)
                .build();
        while (userRepository.existsByUsername(user.getUsername())) {
            user.setUsername(UUID.randomUUID().toString());
        }
        return userRepository.save(user);
    }

    public void processOAuthPostLogin(UserEntity oauthUser) {
        UserEntity user = userRepository.findByUsername(oauthUser.getUsername()).orElse(oauthUser);
        user.setProvider(UserEntity.Provider.GOOGLE); //update provide if needed
        userRepository.save(user);
    }

    public UserEntity getUserByUsername(String username) throws NoSuchElementException{
        return userRepository.findByUsername(username).get();
    }
}
