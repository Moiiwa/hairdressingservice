package hairdressing_svc.service;

import hairdressing_svc.dto.CreateAppointmentDto;
import hairdressing_svc.entity.AppointmentEntity;
import hairdressing_svc.repository.AppointmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AppointmentService {

    private final AppointmentRepository repository;
    private final UserService userService;
    private final BarbershopsService barbershopsService;
    private final BarbersService barbersService;

    public void createAppointment(CreateAppointmentDto createAppointmentDto){
        var appointment = new AppointmentEntity();
        appointment.setBarber(barbersService.getBarberById(createAppointmentDto.getBarberId()));
        appointment.setBarbershop(barbershopsService.getBarbershopById(createAppointmentDto.getBarbershopId()));
        appointment.setUser(userService.getUserByUsername(createAppointmentDto.getUsername()));
        appointment.setTimeSlot(createAppointmentDto.getTimeslot());
        repository.save(appointment);
    }

}
