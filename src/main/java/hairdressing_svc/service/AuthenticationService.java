package hairdressing_svc.service;

import hairdressing_svc.exception.CookieNotFoundException;
import hairdressing_svc.exception.ForbiddenException;
import hairdressing_svc.security.RememberMeServiceImp;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.OAuth2LoginAuthenticationFilter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final HttpServletRequest request;
    private final HttpServletResponse response;
    private final RememberMeServices rememberMeServices;
    private final AuthenticationProvider authenticationProvider;

    private final ClientRegistrationRepository clientRegistrationRepository;
    private HttpSessionOAuth2AuthorizationRequestRepository authorizationRequestRepository;
    private DefaultRedirectStrategy redirectStrategy;
    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
    private OAuth2LoginAuthenticationFilter oAuth2LoginAuthenticationFilter;

    @PostConstruct
    public void setUp() {
        authorizationRequestRepository = new HttpSessionOAuth2AuthorizationRequestRepository();
        redirectStrategy = new DefaultRedirectStrategy();
        oAuth2LoginAuthenticationFilter = new OAuth2LoginAuthenticationFilter(clientRegistrationRepository, oAuth2AuthorizedClientService);
    }

    private void rememberMeAuthenticate() {
        Authentication newAuth = rememberMeServices.autoLogin(request, response);
        if (newAuth != null) {
            try {
                newAuth = authenticationProvider.authenticate(newAuth);
                SecurityContextHolder.getContext().setAuthentication(newAuth);
            } catch (Exception e) {
                throw new CookieNotFoundException("cookie not found");
            }
        } else {
            throw new CookieNotFoundException("cookie not found");
        }
    }

    private void authenticate(String username, String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication newAuth = authenticationProvider.authenticate(authenticationToken);
        rememberMeServices.loginSuccess(request, response, newAuth);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }

    public void login(String username, String password) {
        try {
            rememberMeAuthenticate();
        } catch (AuthenticationException e) {
            try {
                authenticate(username, password);
            } catch (AuthenticationException ignored) {
                throw new ForbiddenException("incorrect credentials");
            }
        }
    }

    public void logout() {
        val auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        if (rememberMeServices instanceof RememberMeServiceImp) {
            ((RememberMeServiceImp) rememberMeServices).dropCookies(response);
        }
    }


    public void googleOAuthRequestAuthentication() throws IOException {
        val clientRegistration = clientRegistrationRepository.findByRegistrationId("google");
        val builder = OAuth2AuthorizationRequest.authorizationCode();
        val authorizationRequest = builder
                .clientId(clientRegistration.getClientId())
                .authorizationUri("https://accounts.google.com/o/oauth2/v2/auth")
                .state(UUID.randomUUID().toString())
                .redirectUri(clientRegistration.getRedirectUri())
                .scope("userinfo.profile")
                .build();
        authorizationRequestRepository.saveAuthorizationRequest(authorizationRequest, request, response);
        redirectStrategy.sendRedirect(request, response, authorizationRequest.getAuthorizationRequestUri());
    }

    public void googleOAuthAuthenticate() {
        val authorizationRequest = authorizationRequestRepository
                .removeAuthorizationRequest(request, response);
        Authentication newAuth = oAuth2LoginAuthenticationFilter.attemptAuthentication(request, response);
        rememberMeServices.loginSuccess(request, response, newAuth);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }
}
