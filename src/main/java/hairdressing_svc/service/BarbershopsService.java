package hairdressing_svc.service;

import hairdressing_svc.dto.BarbershopsRegisterDto;
import hairdressing_svc.dto.CityBarbershopsDto;
import hairdressing_svc.entity.BarbershopEntity;
import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.repository.BarbershopsRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BarbershopsService {

    private final UserService userService;
    private final BarbershopsRepository repository;

    public BarbershopEntity getBarbershopById(Integer id){
        return repository.getByBarbershopsId(id);
    }

    public void createBarbershop(BarbershopsRegisterDto barbershopsRegister){
        UserEntity owner = userService.getUserByUsername(barbershopsRegister.getOwner());
        val barbershop = convertDtoIntoEntity(barbershopsRegister, owner);
        repository.save(barbershop);
    }

    private BarbershopEntity convertDtoIntoEntity(BarbershopsRegisterDto barbershopsRegister, UserEntity owner){
        val barbershop = new BarbershopEntity();
        barbershop.setName(barbershopsRegister.getName());
        barbershop.setPhoneNumber(barbershopsRegister.getPhoneNumber());
        barbershop.setCountry(barbershopsRegister.getCountry());
        barbershop.setRegion(barbershopsRegister.getRegion());
        barbershop.setCity(barbershopsRegister.getCity());
        barbershop.setAddress(barbershopsRegister.getAddress());
        barbershop.setOwner(owner);
        return barbershop;
    }

    public List<BarbershopEntity> getCityBarbershops(CityBarbershopsDto cityBarbershops){
        return repository.getAllByCountryAndRegionAndCity(cityBarbershops.getCountry(),cityBarbershops.getRegion(),cityBarbershops.getCity());
    }
    
}
