package hairdressing_svc.settings;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class SecuritySettings {
    @Value("${hd.security.key}")
    private String key;

    @Value("#{new Boolean(${hd.security.docsProtectionEnabled})}")
    private Boolean docsProtectionEnabled;
}
