package hairdressing_svc.exception;

public class UsernameTakenException extends ForbiddenException {
    public UsernameTakenException(String msg) {
        super(msg);
    }
}
