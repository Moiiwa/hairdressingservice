package hairdressing_svc.exception;

import org.springframework.security.core.AuthenticationException;

public class AnonymousException extends AuthenticationException {
    public AnonymousException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public AnonymousException(String msg) {
        super(msg);
    }
}
