package hairdressing_svc.exception;

public class UserNotFoundException extends ForbiddenException {
    public UserNotFoundException(String msg) {
        super(msg);
    }
}
