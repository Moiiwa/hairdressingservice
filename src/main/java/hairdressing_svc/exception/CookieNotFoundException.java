package hairdressing_svc.exception;


import org.springframework.security.core.AuthenticationException;

public class CookieNotFoundException extends AuthenticationException {
    public CookieNotFoundException(String message) {
        super(message);
    }
}
