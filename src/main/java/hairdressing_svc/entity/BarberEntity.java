package hairdressing_svc.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(
        name = "barber"
)
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BarberEntity {

    @Id
    @Column(name = "barber_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer barberId;

    @JsonIgnore
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "barbershop_id", nullable = false)
    private BarbershopEntity barbershop;

    @Column(name = "name")
    private String name;

    @Column(name = "grade")
    private Double grade;


    @OneToOne
    @MapsId
    @JoinColumn(name = "barber_id")
    private UserEntity user;
}
