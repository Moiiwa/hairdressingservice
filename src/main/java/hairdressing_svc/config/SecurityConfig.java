package hairdressing_svc.config;

import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.security.AuthenticationFailureHandlerImp;
import hairdressing_svc.security.CustomAuthenticationFilter;
import hairdressing_svc.security.RememberMeServiceImp;
import hairdressing_svc.service.UserDetailsServiceImp;
import hairdressing_svc.settings.SecuritySettings;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebMvc
@EnableSwagger2
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    @Autowired
    private SecuritySettings securitySettings;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private Environment environment;

    private final BasicAuthenticationEntryPoint basicAuthEntryPoint = new BasicAuthenticationEntryPoint();

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private static final String[] SECURITY_ROLES = new String[]{
            UserEntity.Role.ROLE_USER.getSecurityRole(),
            UserEntity.Role.ROLE_ANON_USER.getSecurityRole(),
            UserEntity.Role.ROLE_SWAGGER.getSecurityRole(),
    };
    private static final String[] ANON_USER_PATTERNS = new String[]{};
    private static final String[] USER_PATTERNS = new String[]{
            "/barbershops/**",
            "/barbers/**",
            "appointments/**",
    };
    private static final String[] UNPROTECTED_PATTERNS = new String[]{
            "/user/login",
            "/user/login/google",
            "/user/register",
            "/user/generate",
    };
    private static final String[] DOCUMENTATION_PATTERNS = new String[]{
            "/api/v2/api-docs",
            "/api/configuration/ui",
            "/api/swagger-resources/**",
            "/api/configuration/security",
            "/api/swagger-ui.html",
            "/api/webjars/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/webjars/**",
    };


    private void configureDocsAccess(
            HttpSecurity http
    ) throws Exception {
        BasicAuthenticationFilter filter = new CustomAuthenticationFilter(
                authenticationManagerBean(), basicAuthEntryPoint, DOCUMENTATION_PATTERNS
        );
        http
                .authorizeRequests()
                .antMatchers(DOCUMENTATION_PATTERNS).hasRole(UserEntity.Role.ROLE_SWAGGER.getSecurityRole())
                .and().addFilterAt(filter, BasicAuthenticationFilter.class);
    }

    @Override
    protected void configure(HttpSecurity security) throws Exception {
        if (environment.acceptsProfiles("swagger"))
            this.configureDocsAccess(security);
        AuthenticationEntryPoint authFailedEntryPoint = new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
        security
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(ANON_USER_PATTERNS).hasRole(UserEntity.Role.ROLE_ANON_USER.getSecurityRole())
                .antMatchers(USER_PATTERNS).hasRole(UserEntity.Role.ROLE_USER.getSecurityRole())
                .antMatchers("/**").hasAnyRole(SECURITY_ROLES)
                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authFailedEntryPoint)
                .and().logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
                .and().rememberMe().rememberMeServices(rememberMeServices()).key(securitySettings.getKey());
    }

    @Override
    public void configure(WebSecurity webSecurity) {
        webSecurity.ignoring().antMatchers(UNPROTECTED_PATTERNS);
        if (environment.acceptsProfiles("swagger") && !securitySettings.getDocsProtectionEnabled()) {
            webSecurity.ignoring().antMatchers(DOCUMENTATION_PATTERNS);
        }
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/api/v2/api-docs", "/v2/api-docs");
        registry.addRedirectViewController("/api/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
        registry.addRedirectViewController("/api/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
        registry.addRedirectViewController("/api/swagger-resources", "/swagger-resources");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/api/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
        registry.addResourceHandler("/api/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public AuthenticationFailureHandler authFailureHandler() {
        return new AuthenticationFailureHandlerImp();
    }

    @Bean
    public RememberMeServices rememberMeServices() {
        val services = new RememberMeServiceImp(
                securitySettings.getKey(),
                userDetailsService()
        );
        services.setAlwaysRemember(true);
        services.setTokenValiditySeconds(Integer.MAX_VALUE);
        return services;
    }

    @Bean
    public AuthenticationProvider authProvider() {
        val provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(passwordEncoder());
        return provider;
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImp();
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("hairdressing_svc.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}
