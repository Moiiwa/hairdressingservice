package hairdressing_svc.config;

import hairdressing_svc.settings.GoogleSettings;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;

@Configuration
@RequiredArgsConstructor
public class OAuthConfig {
    private final GoogleSettings googleSettings;

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        ClientRegistration googleClient =
                CommonOAuth2Provider.GOOGLE.getBuilder("google")
                        .clientId(this.googleSettings.getClientId())
                        .clientSecret(this.googleSettings.getClientSecret())
                        .redirectUri(this.googleSettings.getRedirectUri())
                        .build();

        return new InMemoryClientRegistrationRepository(googleClient);
    }
}
