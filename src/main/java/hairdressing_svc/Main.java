package hairdressing_svc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("hairdressing_svc.repository")
@EntityScan("hairdressing_svc.entity")
@ComponentScan(basePackages = {"hairdressing_svc", "hairdressing_svc.config", "hairdressing_svc.service", "hairdressing_svc.controller", "hairdressing_svc.settings"})
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}