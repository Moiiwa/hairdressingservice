package hairdressing_svc.security;

import hairdressing_svc.entity.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.persistence.Transient;
import java.util.Collection;

public class SecurityUser extends User {
    @Transient
    private transient final UserEntity userEntity;

    public SecurityUser(
            UserEntity user,
            Collection<? extends GrantedAuthority> authorityList) {
        super(user.getUsername(), user.getPassword(), authorityList);
        this.userEntity = user;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }
}