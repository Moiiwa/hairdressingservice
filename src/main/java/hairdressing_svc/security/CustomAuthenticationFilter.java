package hairdressing_svc.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CustomAuthenticationFilter extends BasicAuthenticationFilter {
    private List<AntPathRequestMatcher> antMatchers;

    public CustomAuthenticationFilter(AuthenticationManager authenticationManager,
                                      AuthenticationEntryPoint authenticationEntryPoint, String[] antPatterns) {
        super(authenticationManager, authenticationEntryPoint);
        this.antMatchers = Arrays.stream(antPatterns).map(AntPathRequestMatcher::new).collect(Collectors.toList());
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (antMatchers.stream().anyMatch(m -> m.matches(request))) {
            super.doFilterInternal(request, response, chain);
        } else {
            chain.doFilter(request, response);
        }
    }
}
