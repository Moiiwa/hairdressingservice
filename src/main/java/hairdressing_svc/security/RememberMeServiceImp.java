package hairdressing_svc.security;

import lombok.val;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class RememberMeServiceImp extends TokenBasedRememberMeServices {
    public RememberMeServiceImp(String key, UserDetailsService userDetailsService) {
        super(key, userDetailsService);
    }

    public void dropCookies(HttpServletResponse response) {
        val cookie = new Cookie(getCookieName(), null);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }
}