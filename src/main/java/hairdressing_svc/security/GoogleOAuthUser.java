package hairdressing_svc.security;

import hairdressing_svc.entity.UserEntity;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Map;
import java.util.UUID;

public class GoogleOAuthUser extends SecurityUser implements OAuth2User {
    private transient final OAuth2User oAuth2User;

    private static UserEntity buildUserEntity(OAuth2User oAuth2User) {
        return UserEntity.builder()
                .username(oAuth2User.getAttribute("email"))
                .password(UUID.randomUUID().toString())
                .name(oAuth2User.getName())
                .role(UserEntity.Role.ROLE_USER)
                .provider(UserEntity.Provider.GOOGLE)
                .build();
    }

    public GoogleOAuthUser(OAuth2User oAuth2User) {
        super(buildUserEntity(oAuth2User), oAuth2User.getAuthorities());
        this.oAuth2User = oAuth2User;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return this.oAuth2User.getAttributes();
    }

    @Override
    public String getName() {
        return this.oAuth2User.getName();
    }
}
