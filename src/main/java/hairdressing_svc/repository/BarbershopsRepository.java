package hairdressing_svc.repository;

import hairdressing_svc.entity.BarbershopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BarbershopsRepository extends JpaRepository<BarbershopEntity, Long> {
    BarbershopEntity getByBarbershopsId(Integer id);

    List<BarbershopEntity> getAllByCountryAndRegionAndCity(String country, String region, String city);
}
