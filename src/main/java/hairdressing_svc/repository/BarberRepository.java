package hairdressing_svc.repository;

import hairdressing_svc.entity.BarberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface BarberRepository extends JpaRepository<BarberEntity, Long> {

    public BarberEntity getBarberEntityByBarberId(Integer barberId);
}
