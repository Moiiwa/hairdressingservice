package hairdressing_svc.repository;

import hairdressing_svc.entity.AppointmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface AppointmentRepository extends JpaRepository<AppointmentEntity, Long> {
}
