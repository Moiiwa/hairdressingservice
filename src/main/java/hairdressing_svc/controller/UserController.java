package hairdressing_svc.controller;

import hairdressing_svc.controller.api.UserApiController;
import hairdressing_svc.dto.UserDto;
import hairdressing_svc.dto.UserLoginDto;
import hairdressing_svc.dto.UserRegisterDto;
import hairdressing_svc.service.AuthenticationService;
import hairdressing_svc.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static hairdressing_svc.util.SecurityUtil.getUser;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApiController {
    private final AuthenticationService authenticationService;
    private final UserService userService;

    @Override
    public ResponseEntity<Void> register(
            @RequestBody UserRegisterDto userRegister
    ) {
        userService.createUser(userRegister);
        return ok().build();
    }

    @Override
    public ResponseEntity<UserDto> login(
            @RequestBody UserLoginDto userLogin
    ) {
        authenticationService.login(userLogin.getUsername(), userLogin.getPassword());
        val user = getUser(SecurityContextHolder.getContext().getAuthentication());
        val userDto = UserDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .phoneNumber(user.getPhoneNumber())
                .username(user.getUsername())
                .build();
        return ok(userDto);
    }

    @Override
    public ResponseEntity<UserDto> generateAnonUser() {
        val user = userService.generateAnonUser();
        val userDto = UserDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .phoneNumber(user.getPhoneNumber())
                .username(user.getUsername())
                .build();
        return ok(userDto);
    }

    @Override
    public ResponseEntity<Void> logout() {
        authenticationService.logout();
        return ok().build();
    }

    @Override
    public ResponseEntity<Void> googleLoginRequest() {
        try {
            authenticationService.googleOAuthRequestAuthentication();
            return ok().build();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public ResponseEntity<Void> googleLoginSuccess() {
        authenticationService.googleOAuthAuthenticate();
        val user = getUser(SecurityContextHolder.getContext().getAuthentication());
        userService.processOAuthPostLogin(user);
        return ok().build();
    }
}

