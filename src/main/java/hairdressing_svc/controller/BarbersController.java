package hairdressing_svc.controller;

import hairdressing_svc.controller.api.BarbersApiController;
import hairdressing_svc.dto.BarberRegisterDto;
import hairdressing_svc.service.BarbersService;
import hairdressing_svc.service.BarbershopsService;
import hairdressing_svc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BarbersController implements BarbersApiController {

    private final BarbersService barbersService;
    private final UserService userService;
    private final BarbershopsService barbershopsService;

    @Override
    public ResponseEntity<Void> registerBarber(@RequestBody BarberRegisterDto barberRegister){
        var user = userService.getUserByUsername(barberRegister.getUsername());
        var barbershop = barbershopsService.getBarbershopById(barberRegister.getBarbershopId());
        barbersService.registerBarber(user, barbershop);
        return ResponseEntity.ok().build();
    }
}
