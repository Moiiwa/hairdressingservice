package hairdressing_svc.controller;

import hairdressing_svc.exception.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ErrorController {
    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @ExceptionHandler({
            IllegalArgumentException.class,
            ServletRequestBindingException.class,  // if required header is missing
            HttpMessageNotReadableException.class,
            MethodArgumentTypeMismatchException.class
    })
    public void onNotValidRequest(HttpServletResponse response, Exception e) throws IOException {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler({
            AuthenticationException.class
    })
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws ServletException, IOException {
        authenticationFailureHandler.onAuthenticationFailure(request, response, e);
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
    }

    @ExceptionHandler({
            ForbiddenException.class
    })
    public void onForbidden(HttpServletResponse response, Exception e) throws IOException {
        response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
    }

    @ExceptionHandler({
            RuntimeException.class
    })
    public void onInternalServerFailure(HttpServletResponse response, Exception e) throws IOException {
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
    }

    @ExceptionHandler({
            NoSuchElementException.class
    })
    public void onAbsentUsernameUsage(HttpServletResponse response, Exception e) throws IOException {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
    }
}
