package hairdressing_svc.controller;

import hairdressing_svc.controller.api.BarbershopApiController;
import hairdressing_svc.dto.BarbershopsRegisterDto;
import hairdressing_svc.dto.CityBarbershopsDto;
import hairdressing_svc.entity.BarbershopEntity;
import hairdressing_svc.service.BarbershopsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
public class BarbershopController implements BarbershopApiController {

    private final BarbershopsService barbershopsService;


    @Override
    public ResponseEntity<Void> register(@RequestBody BarbershopsRegisterDto barbershopsRegister) {
        barbershopsService.createBarbershop(barbershopsRegister);
        return ok().build();
    }

    @Override
    public ResponseEntity<List<BarbershopEntity>> getBarbershopsInCity(CityBarbershopsDto cityBarbershops){
        return ok(barbershopsService.getCityBarbershops(cityBarbershops));
    }
}
