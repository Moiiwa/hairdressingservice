package hairdressing_svc.controller;

import hairdressing_svc.controller.api.AppointmentsApiController;
import hairdressing_svc.dto.CreateAppointmentDto;
import hairdressing_svc.service.AppointmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AppointmentsController implements AppointmentsApiController {
    private final AppointmentService appointmentService;

    @Override
    public ResponseEntity<Void> createAppointment(@RequestBody CreateAppointmentDto createAppointmentDto){
        appointmentService.createAppointment(createAppointmentDto);
        return ResponseEntity.ok().build();
    }
}
