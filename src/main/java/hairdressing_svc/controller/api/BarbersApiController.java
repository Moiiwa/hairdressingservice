package hairdressing_svc.controller.api;

import hairdressing_svc.dto.BarberRegisterDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Api(
        tags = {"barbers"}
)
public interface BarbersApiController {

    @ApiOperation(
            value = "Barber registration", nickname = "register")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 403, message = "Forbidden")
            }
    )
    @RequestMapping(value = "/barbers/register", consumes = {"application/json"}, method = POST)
    ResponseEntity<Void> registerBarber(@RequestBody BarberRegisterDto barberRegister);
}
