package hairdressing_svc.controller.api;

import hairdressing_svc.dto.BarbershopsRegisterDto;
import hairdressing_svc.dto.CityBarbershopsDto;
import hairdressing_svc.entity.BarbershopEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Api(
        tags = {"barbershops"}
)
public interface BarbershopApiController {

    @ApiOperation(
            value = "Barbershop registration", nickname = "register")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 403, message = "Forbidden")
            }
    )
    @RequestMapping(value = "/barbershops/register", consumes = {"application/json"}, method = POST)
    ResponseEntity<Void> register(
            @ApiParam(value = "name, phone number, country, region, city, address")
            @RequestBody BarbershopsRegisterDto userRegister
    );


    @ApiOperation(
            value = "Get barbershops by country, city and region", nickname = "get_by_city")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 403, message = "Forbidden")
            }
    )
    @RequestMapping(value = "/barbershops/get_by_city", consumes = {"application/json"}, method = GET)
    ResponseEntity<List<BarbershopEntity>> getBarbershopsInCity(
            @ApiParam(value = "country, region, city")
            @RequestBody CityBarbershopsDto cityBarbershops
    );
}
