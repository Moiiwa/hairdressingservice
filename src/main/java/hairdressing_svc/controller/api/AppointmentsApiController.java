package hairdressing_svc.controller.api;

import hairdressing_svc.dto.CreateAppointmentDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Api(
        tags = {"appointments"}
)
public interface AppointmentsApiController {

    @ApiOperation(
            value = "Creating an appointment", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 403, message = "Forbidden")
            }
    )
    @RequestMapping(value = "/appointments/create", consumes = {"application/json"}, method = POST)

    ResponseEntity<Void> createAppointment(CreateAppointmentDto createAppointmentDto);
}
