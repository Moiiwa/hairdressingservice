package hairdressing_svc.controller.api;

import hairdressing_svc.dto.UserDto;
import hairdressing_svc.dto.UserLoginDto;
import hairdressing_svc.dto.UserRegisterDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Api(
        tags = {"users"}
)
public interface UserApiController {
    @ApiOperation(
            value = "User registration", nickname = "register")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 403, message = "Forbidden")
            }
    )
    @RequestMapping(value = "/user/register", consumes = {"application/json"}, method = POST)
    ResponseEntity<Void> register(
            @ApiParam(value = "username, password, name, surname and phone_number (optionally)")
            @RequestBody UserRegisterDto userRegister
    );

    @ApiOperation(
            value = "User logging", nickname = "login")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 401, message = "Unauthorized")
            }
    )
    @RequestMapping(value = "/user/login", consumes = {"application/json"}, method = POST)
    ResponseEntity<UserDto> login(
            @ApiParam(value = "username and password")
            @RequestBody UserLoginDto userLogin
    );

    @ApiOperation(
            value = "User logging through Google OAuth2", nickname = "googleLoginRequest")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 401, message = "Unauthorized")
            }
    )
    @RequestMapping(value = "/user/login/google", consumes = {"application/json"}, method = GET)
    ResponseEntity<Void> googleLoginRequest();

    @ApiOperation(
            value = "Authenticate google user", nickname = "googleLoginSuccess")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 401, message = "Unauthorized")
            }
    )
    @RequestMapping(value = "/user/login/google/success", consumes = {"application/json"}, method = POST)
    ResponseEntity<Void> googleLoginSuccess();

    @ApiOperation(
            value = "Randomly generating anon user", nickname = "generateAnonUser")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
            }
    )
    @RequestMapping(value = "/user/generate", consumes = {"application/json"}, method = POST)
    ResponseEntity<UserDto> generateAnonUser();

    @ApiOperation(
            value = "User logout", nickname = "logout")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Success"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 401, message = "Unauthorized")
            }
    )
    @RequestMapping(value = "/user/logout", consumes = {"application/json"}, method = POST)
    ResponseEntity<Void> logout();
}
