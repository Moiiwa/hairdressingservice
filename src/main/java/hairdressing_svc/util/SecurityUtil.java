package hairdressing_svc.util;

import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.exception.AnonymousException;
import hairdressing_svc.security.SecurityUser;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;

public class SecurityUtil {
    public static UserEntity getUser(Authentication authentication) {
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AnonymousException("Anonymous authentication");
        }
        return ((SecurityUser) authentication.getPrincipal()).getUserEntity();
    }
}
