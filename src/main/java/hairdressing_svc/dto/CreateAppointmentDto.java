package hairdressing_svc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateAppointmentDto {

    @JsonProperty(value = "username", required = true)
    private String username;

    @JsonProperty(value = "barbershop_id", required = true)
    private Integer barbershopId;

    @JsonProperty(value = "barber_id", required = true)
    private Integer barberId;

    @JsonProperty(value = "timeslot", required = true)
    private Timestamp timeslot;
}
