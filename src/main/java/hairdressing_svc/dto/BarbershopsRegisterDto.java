package hairdressing_svc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BarbershopsRegisterDto {

    @JsonProperty(value = "name", required = true)
    private String name;

    @JsonProperty(value = "phone_number", required = true)
    private String phoneNumber;

    @JsonProperty(value = "country", required = true)
    private String country;

    @JsonProperty(value = "region", required = true)
    private String region;

    @JsonProperty(value = "city", required = true)
    private String city;

    @JsonProperty(value = "address", required = true)
    private String address;

    @JsonProperty(value = "owner", required = true)
    private String owner;
}
