package hairdressing_svc.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BarberRegisterDto {

    @JsonProperty(value = "username", required = true)
    private String username;
    @JsonProperty(value = "barbershop_id")
    private Integer barbershopId;

}
