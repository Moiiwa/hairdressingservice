package hairdressing_svc;


import hairdressing_svc.dto.CreateAppointmentDto;
import hairdressing_svc.entity.AppointmentEntity;
import hairdressing_svc.entity.BarberEntity;
import hairdressing_svc.entity.BarbershopEntity;
import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.repository.AppointmentRepository;
import hairdressing_svc.service.AppointmentService;
import hairdressing_svc.service.BarbersService;
import hairdressing_svc.service.BarbershopsService;
import hairdressing_svc.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentServiceTests {

    @InjectMocks
    AppointmentService subj;

    @Mock
    AppointmentRepository repository;

    @Mock
    UserService userService;

    @Mock
    BarbershopsService barbershopsService;

    @Mock
    BarbersService barbersService;

    @Test
    public void createAppointmentTest(){
        var createAppointmentsDto = new CreateAppointmentDto();
        var user = new UserEntity();
        var barbershop = new BarbershopEntity();
        var barber = new BarberEntity();
        Mockito.when(userService.getUserByUsername(Mockito.any())).thenReturn(user);
        Mockito.when(barbershopsService.getBarbershopById(Mockito.any())).thenReturn(barbershop);
        Mockito.when(barbersService.getBarberById(Mockito.any())).thenReturn(barber);
        subj.createAppointment(createAppointmentsDto);
        Mockito.verify(repository).save(Mockito.any(AppointmentEntity.class));
    }
}
