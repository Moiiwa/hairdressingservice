package hairdressing_svc;

import hairdressing_svc.dto.UserRegisterDto;
import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.exception.UsernameTakenException;
import hairdressing_svc.repository.UserRepository;
import hairdressing_svc.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    UserService subj;

    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;

    @Test(expected = UsernameTakenException.class)
    public void createUserFailsTest(){
        var userRegisterDto = new UserRegisterDto();
        userRegisterDto.setUsername("moiwa");
        Mockito.when(userRepository.existsByUsername(userRegisterDto.getUsername())).thenReturn(true);
        subj.createUser(userRegisterDto);
    }

    @Test
    public void createUserWorksProperly(){
        var userRegisterDto = new UserRegisterDto();
        userRegisterDto.setUsername("moiwa");
        userRegisterDto.setPassword("password");
        Mockito.when(userRepository.existsByUsername(userRegisterDto.getUsername())).thenReturn(false);
        Mockito.when(passwordEncoder.encode(Mockito.any())).thenReturn("password");
        subj.createUser(userRegisterDto);
        Mockito.verify(userRepository).save(Mockito.any(UserEntity.class));
    }

    @Test
    public void generateAnonUserTest(){
        Mockito.when(userRepository.existsByUsername(Mockito.any())).thenReturn(false);
        subj.generateAnonUser();
        Mockito.verify(userRepository).save(Mockito.any(UserEntity.class));
    }

    @Test
    public void processOAuthPostLoginTest(){
        var user = new UserEntity();
        user.setUsername("moiwa");
        Mockito.when(userRepository.findByUsername("moiwa")).thenReturn(Optional.of(user));
        subj.processOAuthPostLogin(user);
        assertEquals(UserEntity.Provider.GOOGLE, user.getProvider());
        Mockito.verify(userRepository).save(Mockito.any(UserEntity.class));
    }

    @Test
    public void getUserByUsernameTest(){
        var username = "moiwa";
        var userEntity = new UserEntity();
        userEntity.setUsername(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(Optional.of(userEntity));
        var result = subj.getUserByUsername(username);
        assertEquals(userEntity, result);
    }

}