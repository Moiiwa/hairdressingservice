package hairdressing_svc;

import hairdressing_svc.entity.UserEntity;
import hairdressing_svc.repository.UserRepository;
import hairdressing_svc.service.UserDetailsServiceImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImpTest {

    @InjectMocks
    UserDetailsServiceImp subj;

    @Mock
    UserRepository userRepository;

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsernameThrowsException() {
        var optionalUserEntity = Optional.empty();
        Mockito.doReturn(optionalUserEntity).when(userRepository).findByUsername("moiwa");
        subj.loadUserByUsername("moiwa");
    }

    @Test
    public void loadUserByUsername() {
        var userEntity = new UserEntity();
        userEntity.setUsername("moiwa");
        userEntity.setPassword("password");
        userEntity.setRole(UserEntity.Role.ROLE_USER);
        var optionalUserEntity = Optional.of(userEntity);
        Mockito.doReturn(optionalUserEntity).when(userRepository).findByUsername("moiwa");
        var securityUser = subj.loadUserByUsername("moiwa");
        assertEquals(securityUser.getUsername(), userEntity.getUsername());
        assertEquals(securityUser.getPassword(), userEntity.getPassword());
        assertEquals(1, securityUser.getAuthorities().size());
        assertTrue(securityUser.getAuthorities().contains(new SimpleGrantedAuthority(UserEntity.Role.ROLE_USER.name())));
    }

}