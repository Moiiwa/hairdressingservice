sudo apt update
sudo apt install docker.io -y
sudo apt install docker-compose -y
pkill -f 'java -jar'
sudo docker system prune --all -f
sudo docker login -u "$DOCKER_HUB_LOGIN" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
sudo TAG="$TAG" docker-compose up -d